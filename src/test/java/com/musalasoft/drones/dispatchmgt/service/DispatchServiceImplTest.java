/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dispatchmgt.service;

import com.musalasoft.drones.audit.repository.AuditEventRepository;
import com.musalasoft.drones.constants.ResponseConstant;
import com.musalasoft.drones.dispatchmgt.dto.DispatchCreateRecordRequest;
import com.musalasoft.drones.dispatchmgt.enums.DispatchStatus;
import com.musalasoft.drones.dispatchmgt.model.DispatchHistory;
import com.musalasoft.drones.dispatchmgt.repository.DispatchHistoryRepository;
import com.musalasoft.drones.dronemgt.enums.Model;
import com.musalasoft.drones.dronemgt.enums.State;
import com.musalasoft.drones.dronemgt.model.Drone;
import com.musalasoft.drones.dronemgt.repository.DroneRepository;
import com.musalasoft.drones.dto.ResponseDto;
import com.musalasoft.drones.medicationmgt.model.Medication;
import com.musalasoft.drones.medicationmgt.repository.MedicationRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

class DispatchServiceImplTest {
    @Mock
    DroneRepository droneRepository;
    @Mock
    DispatchHistoryRepository dispatchHistoryRepository;
    @Mock
    MedicationRepository medicationRepository;

    @Mock
    AuditEventRepository auditEventRepository;
    @Mock
    Logger log;
    @InjectMocks
    DispatchServiceImpl dispatchServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testCreateRecord() {
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setWeightLimit(200);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setSerialNumber("111");

        Medication medication = new Medication();
        medication.setName("Panadol");
        medication.setWeight(50);
        medication.setImage("sample image string");
        medication.setCode("222");
        when(droneRepository.findFirstBySerialNumber("111")).thenReturn(drone);
        when(dispatchHistoryRepository.findByDroneSerialNoAndStatus(anyString(), any())).thenReturn(null);
        when(dispatchHistoryRepository.existsByDroneSerialNoAndStatus(anyString(), any())).thenReturn(false);
        when(medicationRepository.findByCode("222")).thenReturn(medication);
        DispatchCreateRecordRequest recordRequest = new DispatchCreateRecordRequest();
        recordRequest.setMedicationCode("222");
        recordRequest.setDroneSerialNo("111");
        ResponseDto result = dispatchServiceImpl.createRecord(recordRequest);
        log.info(result.toString());
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }

    @Test
    void testCreateRecord2() {
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setWeightLimit(200);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setSerialNumber("111");

        Medication medication = new Medication();
        medication.setName("Panadol");
        medication.setWeight(50);
        medication.setImage("sample image string");
        medication.setCode("222");


        List<Medication> medicationList = new ArrayList<>();
        medicationList.add(medication);
        DispatchHistory dispatchHistory = new DispatchHistory();
        dispatchHistory.setMedicationItems(medicationList);
        dispatchHistory.setStatus(DispatchStatus.PROCESSING);
        dispatchHistory.setDispatchNo(RandomStringUtils.randomAlphabetic(10));
        dispatchHistory.setDroneSerialNo(drone.getSerialNumber());
        dispatchHistory.setTotalWeight(medication.getWeight());


        when(droneRepository.findFirstBySerialNumber("111")).thenReturn(drone);
        when(dispatchHistoryRepository.findByDroneSerialNoAndStatus("111", DispatchStatus.PROCESSING)).thenReturn(dispatchHistory);
        when(dispatchHistoryRepository.existsByDroneSerialNoAndStatus("111", DispatchStatus.PROCESSING)).thenReturn(true);
        when(medicationRepository.findByCode("222")).thenReturn(medication);
        DispatchCreateRecordRequest recordRequest = new DispatchCreateRecordRequest();
        recordRequest.setMedicationCode("222");
        recordRequest.setDroneSerialNo("111");
        ResponseDto result = dispatchServiceImpl.createRecord(recordRequest);

        log.info(result.toString());
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }

    @Test
    void testFetchRecordByDrone() {
        Medication medication = new Medication();
        medication.setName("Panadol");
        medication.setWeight(50);
        medication.setImage("sample image string");
        medication.setCode("222");


        List<Medication> medicationList = new ArrayList<>();
        medicationList.add(medication);
        DispatchHistory dispatchHistory = new DispatchHistory();
        dispatchHistory.setMedicationItems(medicationList);
        dispatchHistory.setStatus(DispatchStatus.PROCESSING);
        dispatchHistory.setDispatchNo(RandomStringUtils.randomAlphabetic(10));
        dispatchHistory.setDroneSerialNo("111");
        dispatchHistory.setTotalWeight(medication.getWeight());
        when(dispatchHistoryRepository.findByDroneSerialNoAndStatus(anyString(), any())).thenReturn(dispatchHistory);

        ResponseDto result = dispatchServiceImpl.fetchRecordByDrone("111");
        log.info(result.toString());
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }
}
