/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dronemgt.service;

import com.musalasoft.drones.audit.repository.AuditEventRepository;
import com.musalasoft.drones.constants.ResponseConstant;
import com.musalasoft.drones.dronemgt.dto.DroneRegisterRequest;
import com.musalasoft.drones.dronemgt.dto.DroneUpdateRequest;
import com.musalasoft.drones.dronemgt.enums.Model;
import com.musalasoft.drones.dronemgt.enums.State;
import com.musalasoft.drones.dronemgt.model.Drone;
import com.musalasoft.drones.dronemgt.projection.DroneBatteryInfo;
import com.musalasoft.drones.dronemgt.repository.DroneRepository;
import com.musalasoft.drones.dto.ResponseDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;

import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

class DroneServiceImplTest {
    @Mock
    DroneRepository droneRepository;
    @Mock
    ModelMapper mapper;
    @Mock
    AuditEventRepository auditEventRepository;
    @Mock
    Logger log;
    @InjectMocks
    DroneServiceImpl droneServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testRegister() {
        DroneRegisterRequest registerRequest = new DroneRegisterRequest();
        registerRequest.setSerialNumber("111");
        registerRequest.setBatteryCapacity(100);
        registerRequest.setModel(Model.Lightweight);
        registerRequest.setState(State.IDLE);
        registerRequest.setWeightLimit(300);

        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setWeightLimit(200);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setSerialNumber("111");

        when(droneRepository.save(any())).thenReturn(drone);
        ResponseDto result = droneServiceImpl.register(registerRequest);
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }

    @Test
    void testUpdate() {
        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setWeightLimit(200);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setSerialNumber("111");
        when(droneRepository.findFirstBySerialNumber("111")).thenReturn(drone);

        DroneUpdateRequest droneUpdateRequest = new DroneUpdateRequest();
        droneUpdateRequest.setBatteryCapacity(30);
        droneUpdateRequest.setSerialNumber("111");
        droneUpdateRequest.setState(State.LOADED);
        droneUpdateRequest.setModel(Model.Middleweight);
        ResponseDto result = droneServiceImpl.update(droneUpdateRequest);
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }

    @Test
    void testFetchByState() {

        Drone drone = new Drone();
        drone.setState(State.IDLE);
        drone.setWeightLimit(200);
        drone.setModel(Model.Lightweight);
        drone.setBatteryCapacity(100);
        drone.setSerialNumber("111");

        when(droneRepository.findByState(State.IDLE)).thenReturn(List.of(drone));

        ResponseDto result = droneServiceImpl.fetchByState(State.IDLE);
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }

    @Test
    void testCheckBatteryLevel() {
        when(droneRepository.findBySerialNumber("111")).thenReturn(new DroneBatteryInfo() {
            @Override
            public String getSerialNumber() {
                return "111";
            }

            @Override
            public Integer getBatteryCapacity() {
                return 80;
            }
        });

        ResponseDto result = droneServiceImpl.checkBatteryLevel("111");
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }
}
