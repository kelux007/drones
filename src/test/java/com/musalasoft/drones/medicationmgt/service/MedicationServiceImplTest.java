/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.medicationmgt.service;

import com.musalasoft.drones.audit.repository.AuditEventRepository;
import com.musalasoft.drones.constants.ResponseConstant;
import com.musalasoft.drones.dto.ResponseDto;
import com.musalasoft.drones.medicationmgt.dto.MedicationRegisterRequest;
import com.musalasoft.drones.medicationmgt.dto.MedicationUpdateRequest;
import com.musalasoft.drones.medicationmgt.model.Medication;
import com.musalasoft.drones.medicationmgt.repository.MedicationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

class MedicationServiceImplTest {
    @Mock
    MedicationRepository medicationRepository;
    @Mock
    ModelMapper mapper;
    @Mock
    AuditEventRepository auditEventRepository;
    @Mock
    Logger log;
    @InjectMocks
    MedicationServiceImpl medicationServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void testRegister() {
        MedicationRegisterRequest request = new MedicationRegisterRequest();
        request.setCode("222");
        request.setName("Panadol");
        request.setWeight(50);
        request.setImage("sample image string");

        Medication medication = new Medication();
        medication.setName("Panadol");
        medication.setWeight(50);
        medication.setImage("sample image string");
        medication.setCode("222");

        when(medicationRepository.save(any())).thenReturn(medication);
        ResponseDto result = medicationServiceImpl.register(request);
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }

    @Test
    void testUpdate() {
        Medication medication = new Medication();
        medication.setName("Panadol");
        medication.setWeight(50);
        medication.setImage("sample image string");
        medication.setCode("222");
        when(medicationRepository.findByCode("222")).thenReturn(medication);

        MedicationUpdateRequest request = new MedicationUpdateRequest();
        request.setCode("222");
        request.setName("Panadol");
        request.setWeight(50);
        request.setImage("sample image string");

        ResponseDto result = medicationServiceImpl.update(request);
        Assertions.assertEquals(ResponseConstant.SUCCESS_CODE, result.getRespCode());
    }
}

