/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/22/22, 10:43 PM
 *
 */

package com.musalasoft.drones;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DronesApplicationTests {

	@Test
	void contextLoads() {
	}

}
