/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.jobrunner;


import com.musalasoft.drones.audit.enums.AuditType;
import com.musalasoft.drones.audit.model.AuditEvent;
import com.musalasoft.drones.audit.repository.AuditEventRepository;
import com.musalasoft.drones.dronemgt.model.Drone;
import com.musalasoft.drones.dronemgt.repository.DroneRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class BatteryChecker {
    private final DroneRepository droneRepository;
    private final AuditEventRepository auditEventRepository;

//    @Scheduled(fixedRate = 60*1000)
    private void checkBatteryLevel(){
        log.info("logging drone battery level at {}:", new Date());
        List<Drone> drones = droneRepository.findAll();
        for (int i = 0; i < drones.size(); i++) {
            AuditEvent auditEvent =  new AuditEvent();
            auditEvent.setType(AuditType.BATTERY_LEVEL);
            auditEvent.setDetails(String.format("Battery level for drone:%s is %d%%", drones.get(i).getSerialNumber(), drones.get(i).getBatteryCapacity()));
            auditEventRepository.save(auditEvent);
        }


    }

}
