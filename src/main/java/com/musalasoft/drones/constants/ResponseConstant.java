/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 3:06 PM
 *
 */

package com.musalasoft.drones.constants;

public class ResponseConstant {

    public static final String SUCCESS_CODE = "00";
    public static final String FAILED_CODE = "96";
    public static final String DRONE_REGISTERED = "DRONE_REGISTERED";

    public static final String MEDICATION_REGISTERED = "MEDICATION_REGISTERED";
    public static final String DRONE_REGISTRATION_FAILED = "DRONE_REGISTRATION_FAILED";

    public static final String MEDICATION_REGISTRATION_FAILED = "MEDICATION_REGISTRATION_FAILED";
    public static final String DRONE_UPDATED= "DRONE_UPDATED";
    public static final String DRONE_UPDATE_FAILED = "DRONE_UPDATE_FAILED";

    public static final String MEDICATION_UPDATED = "MEDICATION_UPDATED";
    public static final String MEDICATION_UPDATE_FAILED = "MEDICATION_UPDATE_FAILED";

    public static final String RECORD_FETCHED = "RECORD_FETCHED";
    public static final String RECORD_FETCHED_FAILED = "RECORD_FETCHED_FAILED";
    public static final String RECORD_NOT_FOUND = "RECORD_NOT_FOUND";

    public static final String DISPATCH_RECORD_CREATED = "DISPATCH_RECORD_CREATED";
    public static final String DISPATCH_RECORD_CREATION_FAILED = "DISPATCH_RECORD_CREATION_FAILED";
    public static final String DRONE_WEIGHT_LIMIT_EXCEEDED = "DRONE_WEIGHT_LIMIT_EXCEEDED";
    public static final String BATTERY_LOW = "BATTERY_LOW";



    public static final String DRONE_UNAVAILABLE = "DRONE_UNAVAILABLE";
}
