/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 4:14 PM
 *
 */

package com.musalasoft.drones.dispatchmgt.repository;

import com.musalasoft.drones.dispatchmgt.enums.DispatchStatus;
import com.musalasoft.drones.dispatchmgt.model.DispatchHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DispatchHistoryRepository extends JpaRepository<DispatchHistory, Long> {
    @Query(value = "select dh from DispatchHistory dh where dh.droneSerialNo=:serialNo and dh.status=:status ")
    DispatchHistory findByDroneSerialNoAndStatus(String serialNo, DispatchStatus status);

    boolean existsByDroneSerialNoAndStatus(String serialNo, DispatchStatus status);


}
