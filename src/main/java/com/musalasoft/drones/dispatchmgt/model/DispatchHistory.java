/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dispatchmgt.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.musalasoft.drones.abstractmodel.AbstractEntity;
import com.musalasoft.drones.dispatchmgt.enums.DispatchStatus;
import com.musalasoft.drones.medicationmgt.model.Medication;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "dispatch_history")
@ToString(callSuper = true)
public class DispatchHistory extends AbstractEntity {

    protected String droneSerialNo;

    @ManyToMany(fetch = FetchType.EAGER)
    protected List<Medication> medicationItems;

    protected Integer totalWeight;

    @Enumerated(EnumType.STRING)
    protected DispatchStatus status;

    @Column(unique = true)
    protected String dispatchNo;







}
