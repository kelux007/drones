/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dispatchmgt.dto;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class DispatchCreateRecordRequest {

    @NotBlank(message = "Drone serial number is required")
    protected String droneSerialNo;

    @NotNull(message = "medication code is required")
    protected String medicationCode;
}
