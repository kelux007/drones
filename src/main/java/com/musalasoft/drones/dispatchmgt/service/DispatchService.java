/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/23/22, 4:28 AM
 *
 */

package com.musalasoft.drones.dispatchmgt.service;

import com.musalasoft.drones.dispatchmgt.dto.DispatchCreateRecordRequest;
import com.musalasoft.drones.dto.ResponseDto;

public interface DispatchService {
    ResponseDto createRecord(DispatchCreateRecordRequest createRecordRequest);

    ResponseDto fetchRecordByDrone(String droneSerialNo);
}
