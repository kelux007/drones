/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dispatchmgt.service;


import com.musalasoft.drones.audit.enums.AuditType;
import com.musalasoft.drones.audit.model.AuditEvent;
import com.musalasoft.drones.audit.repository.AuditEventRepository;
import com.musalasoft.drones.constants.ResponseConstant;
import com.musalasoft.drones.dispatchmgt.dto.DispatchCreateRecordRequest;
import com.musalasoft.drones.dispatchmgt.enums.DispatchStatus;
import com.musalasoft.drones.dispatchmgt.model.DispatchHistory;
import com.musalasoft.drones.dispatchmgt.repository.DispatchHistoryRepository;
import com.musalasoft.drones.dronemgt.enums.State;
import com.musalasoft.drones.dronemgt.model.Drone;
import com.musalasoft.drones.dronemgt.repository.DroneRepository;
import com.musalasoft.drones.dto.ResponseDto;
import com.musalasoft.drones.medicationmgt.model.Medication;
import com.musalasoft.drones.medicationmgt.repository.MedicationRepository;
import com.musalasoft.drones.util.LocaleHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class DispatchServiceImpl implements DispatchService{

    private final DroneRepository droneRepository;
    private final DispatchHistoryRepository dispatchHistoryRepository;

    private final MedicationRepository medicationRepository;

    private final AuditEventRepository auditEventRepository;

    @Override
    public ResponseDto createRecord(DispatchCreateRecordRequest createRecordRequest) {
       Drone drone = droneRepository.findFirstBySerialNumber(createRecordRequest.getDroneSerialNo());
        if(Objects.isNull(drone)){
            return ResponseDto.builder()
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.DISPATCH_RECORD_CREATION_FAILED))
                    .respCode(ResponseConstant.FAILED_CODE)
                    .build();
        }

        if(drone.getBatteryCapacity()<25){
            return ResponseDto.builder()
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.BATTERY_LOW))
                    .respCode(ResponseConstant.FAILED_CODE)
                    .build();
        }

        if(!drone.getState().equals(State.IDLE) && !drone.getState().equals(State.LOADING)){
            return ResponseDto.builder()
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.DRONE_UNAVAILABLE))
                    .respCode(ResponseConstant.FAILED_CODE)
                    .build();
        }


        Medication medication = medicationRepository.findByCode(createRecordRequest.getMedicationCode());
        Integer sumOfMedicationWeight = 0;
        boolean checkIfDispatchExist =dispatchHistoryRepository.existsByDroneSerialNoAndStatus(createRecordRequest.getDroneSerialNo(), DispatchStatus.PROCESSING);
        DispatchHistory dispatchHistory = new DispatchHistory();
        List<Medication> medicationList = new ArrayList<>();
        if(checkIfDispatchExist){
            dispatchHistory = dispatchHistoryRepository.findByDroneSerialNoAndStatus(createRecordRequest.getDroneSerialNo(), DispatchStatus.PROCESSING);
            sumOfMedicationWeight = medication.getWeight() + dispatchHistory.getTotalWeight();
            medicationList = dispatchHistory.getMedicationItems();
        }else {
            sumOfMedicationWeight = medication.getWeight();
            dispatchHistory.setDispatchNo(RandomStringUtils.randomAlphabetic(10));
        }

        if(sumOfMedicationWeight> drone.getWeightLimit()){
            return ResponseDto.builder()
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.DRONE_WEIGHT_LIMIT_EXCEEDED) + drone.getWeightLimit() + "gr")
                    .respCode(ResponseConstant.FAILED_CODE)
                    .build();
        }

        drone.setState(State.LOADING);
        droneRepository.save(drone);

        dispatchHistory.setStatus(DispatchStatus.PROCESSING);
        dispatchHistory.setDroneSerialNo(createRecordRequest.getDroneSerialNo());
        medicationList.add(medication);
        dispatchHistory.setMedicationItems(medicationList);
        dispatchHistory.setTotalWeight(sumOfMedicationWeight);
        DispatchHistory savedDispatch=dispatchHistoryRepository.save(dispatchHistory);

        AuditEvent auditEvent = new AuditEvent();
        auditEvent.setType(AuditType.CREATE_DISPATCH);
        auditEvent.setDetails(dispatchHistory.toString());
        auditEventRepository.save(auditEvent);

        return ResponseDto.builder()
                .respDescription(LocaleHandler.getMessage(ResponseConstant.DISPATCH_RECORD_CREATED))
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respBody(savedDispatch)
                .build();
    }

    @Override
    public ResponseDto fetchRecordByDrone(String droneSerialNo) {
        DispatchHistory dispatchHistory = dispatchHistoryRepository.findByDroneSerialNoAndStatus(droneSerialNo, DispatchStatus.PROCESSING);
        if(Objects.isNull(dispatchHistory)){
            return ResponseDto.builder()
                    .respCode(ResponseConstant.FAILED_CODE)
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.RECORD_NOT_FOUND,null))
                    .build();
        }

        return ResponseDto.builder()
                .respDescription(LocaleHandler.getMessage(ResponseConstant.RECORD_FETCHED))
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respBody(dispatchHistory)
                .build();
    }
}
