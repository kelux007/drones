/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/23/22, 3:59 AM
 *
 */

package com.musalasoft.drones.dispatchmgt.enums;

public enum DispatchStatus {
    PROCESSING, COMPLETED
}
