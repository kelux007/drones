/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dispatchmgt.controller;


import com.musalasoft.drones.dispatchmgt.dto.DispatchCreateRecordRequest;
import com.musalasoft.drones.dispatchmgt.service.DispatchService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("/dispatch")
public class DispatchController {

    @Autowired
    DispatchService dispatchService;

    @PostMapping("/createRecord")
    ResponseEntity<?> createRecord(@Valid @RequestBody DispatchCreateRecordRequest createRecordRequest){
        log.info("Create Dispatch Record request:{}", createRecordRequest);

        try {
            return new ResponseEntity<>(dispatchService.createRecord(createRecordRequest), HttpStatus.CREATED);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


    }


    @GetMapping("/fetch/drone")
    ResponseEntity<?> fetchRecordByDrone(@RequestParam String droneSerialNo){
        log.info("Fetch Dispatch Record for drone with serial number:{}", droneSerialNo);

        try {
            return new ResponseEntity<>(dispatchService.fetchRecordByDrone(droneSerialNo), HttpStatus.OK);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


    }
}
