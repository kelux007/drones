/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 1:50 PM
 *
 */

package com.musalasoft.drones.exception;

public class GeneralException {

    private String logRef;
    private String message;

    public GeneralException(String logRef, String message) {
        super();
        this.logRef = logRef;
        this.message = message;
    }

    public GeneralException() {
        super();
    }

    public String getLogRef() {
        return logRef;
    }

    public void setLogRef(String logRef) {
        this.logRef = logRef;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
