/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.medicationmgt.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.musalasoft.drones.abstractmodel.AbstractEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

@Entity
@Table(name = "medication")
@Setter
@Getter
@ToString(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Medication extends AbstractEntity {

    protected String name;

    protected Integer weight;

    protected String code;

    @Lob
    protected String image;
}
