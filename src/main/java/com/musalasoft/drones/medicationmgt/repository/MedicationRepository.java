/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.medicationmgt.repository;

import com.musalasoft.drones.medicationmgt.model.Medication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MedicationRepository extends JpaRepository<Medication, Long> {
    Medication findByCode(String code);

}
