/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/23/22, 2:56 AM
 *
 */

package com.musalasoft.drones.medicationmgt.service;

import com.musalasoft.drones.dto.ResponseDto;
import com.musalasoft.drones.medicationmgt.dto.MedicationRegisterRequest;
import com.musalasoft.drones.medicationmgt.dto.MedicationUpdateRequest;

public interface MedicationService {
    ResponseDto register(MedicationRegisterRequest medicationRegisterRequest);

    ResponseDto update(MedicationUpdateRequest medicationUpdateRequest);
}
