/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.medicationmgt.service;


import com.musalasoft.drones.audit.enums.AuditType;
import com.musalasoft.drones.audit.model.AuditEvent;
import com.musalasoft.drones.audit.repository.AuditEventRepository;
import com.musalasoft.drones.constants.ResponseConstant;
import com.musalasoft.drones.dto.ResponseDto;
import com.musalasoft.drones.medicationmgt.dto.MedicationRegisterRequest;
import com.musalasoft.drones.medicationmgt.dto.MedicationUpdateRequest;
import com.musalasoft.drones.medicationmgt.model.Medication;
import com.musalasoft.drones.medicationmgt.repository.MedicationRepository;
import com.musalasoft.drones.util.LocaleHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class MedicationServiceImpl implements MedicationService {

    private final MedicationRepository medicationRepository;

    private final ModelMapper mapper;

    private final AuditEventRepository auditEventRepository;

    @Override
    public ResponseDto register(MedicationRegisterRequest medicationRegisterRequest) {
        Medication medication = mapper.map(medicationRegisterRequest, Medication.class);
        Medication savedMedication = medicationRepository.save(medication);

        AuditEvent auditEvent =  new AuditEvent();
        auditEvent.setType(AuditType.CREATE_MEDICATION);
        auditEvent.setDetails(savedMedication.toString());
        auditEventRepository.save(auditEvent);

        return ResponseDto.builder()
                .respBody(savedMedication)
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respDescription(LocaleHandler.getMessage(ResponseConstant.MEDICATION_REGISTERED,null))
                .build();
    }



    @Override
    public ResponseDto update(MedicationUpdateRequest medicationUpdateRequest) {
        Medication medication = medicationRepository.findByCode(medicationUpdateRequest.getCode());
        if(Objects.isNull(medication)){
            return ResponseDto.builder()
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.DRONE_UPDATE_FAILED))
                    .respCode(ResponseConstant.FAILED_CODE)
                    .build();
        }
        if(!Objects.isNull(medicationUpdateRequest.getWeight())){
            medication.setWeight(medicationUpdateRequest.getWeight());
        }
        if(!Objects.isNull(medicationUpdateRequest.getImage())){
            medication.setImage(medicationUpdateRequest.getImage());
        }
        if(!Objects.isNull(medicationUpdateRequest.getName())){
            medication.setName(medicationUpdateRequest.getName());
        }

        Medication updatedMedication = medicationRepository.save(medication);
        return ResponseDto.builder()
                .respBody(updatedMedication)
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respDescription(LocaleHandler.getMessage(ResponseConstant.DRONE_UPDATED,null))
                .build();
    }

}
