/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 4:41 PM
 *
 */

package com.musalasoft.drones.medicationmgt.controller;


import com.musalasoft.drones.medicationmgt.dto.MedicationRegisterRequest;
import com.musalasoft.drones.medicationmgt.dto.MedicationUpdateRequest;
import com.musalasoft.drones.medicationmgt.service.MedicationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping("/medication")
@Validated
public class MedicationController {

    @Autowired
    MedicationService medicationService;




    @PostMapping("/register")
    ResponseEntity<?> registerMedication(@Valid @RequestBody MedicationRegisterRequest medicationRegisterRequest){
        log.info("Register medication request:{}", medicationRegisterRequest);

        try {
            return new ResponseEntity<>(medicationService.register(medicationRegisterRequest), HttpStatus.CREATED);
        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }


    }

    @PostMapping("/update")
    ResponseEntity<?> updateMedication(@Valid @RequestBody MedicationUpdateRequest medicationUpdateRequest){
        log.info("Update Medication request:{}", medicationUpdateRequest);

        try {
            return new ResponseEntity<>(medicationService.update(medicationUpdateRequest), HttpStatus.OK);
        }catch (Exception e){
            throw new RuntimeException(e.getMessage());
        }


    }
}
