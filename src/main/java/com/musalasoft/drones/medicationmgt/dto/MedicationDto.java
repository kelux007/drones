/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 7:25 PM
 *
 */

package com.musalasoft.drones.medicationmgt.dto;

import lombok.Data;

import java.io.Serializable;


@Data
public class MedicationDto implements Serializable {
    private String name;
    private Integer weight;
    private String code;
    private String image;
}
