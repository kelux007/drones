/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.medicationmgt.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.io.Serializable;


@Data
public class MedicationUpdateRequest implements Serializable {

    @Pattern(regexp = "^[A-Za-z0-9_-]*$", message = "Only letters, hyphens underscore and numbers are allowed")
    protected String name;

    protected Integer weight;

    @NotBlank(message = "Code is required")
    @Pattern(regexp = "^[A-Z0-9_]*$", message = "Only upper case letters, underscore and numbers are allowed")
    protected String code;

    protected String image;
}
