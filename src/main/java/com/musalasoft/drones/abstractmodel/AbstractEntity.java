
/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 3:28 PM
 *
 */

package com.musalasoft.drones.abstractmodel;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.modelmapper.ModelMapper;

import javax.persistence.*;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
@MappedSuperclass
public abstract class AbstractEntity implements Serializable, SerializableEntity<AbstractEntity> {

    @Version
    @JsonIgnore
    protected int version;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    @JsonIgnore
    private Long id;

    @JsonIgnore
    protected String delFlag = "N";

    protected LocalDateTime deletedOn;

    @UpdateTimestamp
    protected LocalDateTime updatedOn;

    @CreationTimestamp
    protected LocalDateTime dateCreated = LocalDateTime.now();

    @Override
    public String serialize() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(this);
    }

    @Override
    public void deserialize(String data) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        AbstractEntity readValue = mapper.readValue(data, this.getClass());
        ModelMapper modelMapper = new ModelMapper();
        modelMapper.map(readValue, this);

    }

    @JsonIgnore
    public List<String> getDefaultSearchFields(){
        return new ArrayList<String>();
    }

    @Override
    public String toString() {
        return
                        ", version=" + version +
                        ", delFlag='" + delFlag + '\'' +
                        ", deletedOn=" + deletedOn +
                        ", dateCreated=" + dateCreated +
                        '}';
    }
}
