/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/23/22, 12:04 AM
 *
 */

package com.musalasoft.drones.abstractmodel;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.io.IOException;

/** Any class implementing this interface shall provide methods to
 * convert the {@code object} into JSON GetOffersRequest as well as to
 * convert back to the {@code object} from JSON GetOffersRequest
 *
 * @param <T> The class of the object
 */
public interface SerializableEntity<T> {
    String serialize() throws JsonProcessingException;

    void deserialize(String data) throws IOException;

}
