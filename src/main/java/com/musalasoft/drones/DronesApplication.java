/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 5:22 PM
 *
 */

package com.musalasoft.drones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class DronesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DronesApplication.class, args);
	}

}
