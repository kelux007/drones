/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 6:18 PM
 *
 */

package com.musalasoft.drones.audit.service;

import com.musalasoft.drones.audit.enums.AuditType;
import com.musalasoft.drones.audit.model.AuditEvent;
import com.musalasoft.drones.audit.repository.AuditEventRepository;
import com.musalasoft.drones.constants.ResponseConstant;
import com.musalasoft.drones.dto.ResponseDto;
import com.musalasoft.drones.util.LocaleHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class AuditServiceImpl implements AuditService{

    private final AuditEventRepository auditEventRepository;
    @Override
    public ResponseDto fetchRecordByDrone(AuditType type) {
        List<AuditEvent> auditEvents = new ArrayList<>();
        if(type.equals(AuditType.ALL)){
          auditEvents = auditEventRepository.findAll();
        }else {
            auditEvents = auditEventRepository.findByType(type);
        }

        return ResponseDto.builder()
                .respBody(auditEvents)
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respDescription(LocaleHandler.getMessage(ResponseConstant.MEDICATION_REGISTERED,null))
                .build();
    }
}
