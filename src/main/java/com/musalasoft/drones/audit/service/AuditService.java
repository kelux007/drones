/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 6:18 PM
 *
 */

package com.musalasoft.drones.audit.service;

import com.musalasoft.drones.audit.enums.AuditType;
import com.musalasoft.drones.dto.ResponseDto;

public interface AuditService {
    ResponseDto fetchRecordByDrone(AuditType type);
}
