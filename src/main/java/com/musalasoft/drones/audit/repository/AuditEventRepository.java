/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 6:18 PM
 *
 */

package com.musalasoft.drones.audit.repository;

import com.musalasoft.drones.audit.enums.AuditType;
import com.musalasoft.drones.audit.model.AuditEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuditEventRepository extends JpaRepository<AuditEvent, Long> {

    List<AuditEvent> findByType(AuditType type);
}
