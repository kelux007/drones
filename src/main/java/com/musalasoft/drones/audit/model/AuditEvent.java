/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.audit.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.musalasoft.drones.abstractmodel.AbstractEntity;
import com.musalasoft.drones.audit.enums.AuditType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
@Table(name = "audit_event")
public class AuditEvent extends AbstractEntity {

    @Enumerated(EnumType.STRING)
    protected AuditType type;

    @Lob
    protected String details;

}
