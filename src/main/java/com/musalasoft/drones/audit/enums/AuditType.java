/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 6:18 PM
 *
 */

package com.musalasoft.drones.audit.enums;

public enum AuditType {
    BATTERY_LEVEL,CREATE_DRONE,CREATE_MEDICATION,CREATE_DISPATCH, ALL
}
