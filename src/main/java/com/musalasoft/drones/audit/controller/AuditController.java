/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 6:18 PM
 *
 */

package com.musalasoft.drones.audit.controller;

import com.musalasoft.drones.audit.enums.AuditType;
import com.musalasoft.drones.audit.service.AuditService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/audit")
@Slf4j
public class AuditController {

    @Autowired
    AuditService auditService;


    @GetMapping("/fetch")
    ResponseEntity<?> fetchRecordByDrone(@RequestParam AuditType type){
        log.info("Fetch all audit records for type:{}", type.name());

        try {
            return new ResponseEntity<>(auditService.fetchRecordByDrone(type), HttpStatus.OK);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


    }
}
