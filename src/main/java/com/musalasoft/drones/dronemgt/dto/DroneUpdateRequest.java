/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dronemgt.dto;

import com.musalasoft.drones.dronemgt.enums.Model;
import com.musalasoft.drones.dronemgt.enums.State;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Data

public class DroneUpdateRequest implements Serializable {

    @Size(max = 100, message = "Length cannot be more than 100 characters")
    @NotBlank(message = "Serial Number is required")
    protected String serialNumber;

    protected Model model;

    @Max(value = 500,message = "Maximum weight limit allowed is 500gr")
    protected Integer weightLimit;

    @Max(value = 100, message = "Battery level cannot exceed 100%")
    @Min(value = 0,message = "Battery level cannot go below 0%")
    protected Integer batteryCapacity;

    protected State state;
}
