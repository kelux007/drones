/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 7:25 PM
 *
 */

package com.musalasoft.drones.dronemgt.dto;

import com.musalasoft.drones.dronemgt.enums.Model;
import com.musalasoft.drones.dronemgt.enums.State;
import lombok.Data;

import javax.validation.constraints.*;
import java.io.Serializable;


@Data
public class DroneRegisterRequest implements Serializable {

    @NotBlank(message = "Serial Number is required")
    @Size(max = 100, message = "Length cannot be more than 100 characters")
    protected String serialNumber;

    @NotNull(message = "Model is required")
    protected Model model;

    @NotNull(message = "Weight Limit is required")
    @Max(value = 500,message = "Maximum weight limit allowed is 500gr")
    protected Integer weightLimit;

    @NotNull(message = "Battery Capacity is required")
    @Max(value = 100, message = "Battery level cannot exceed 100%")
    @Min(value = 0,message = "Battery level cannot go below 0%")
    protected Integer batteryCapacity;

    @NotNull(message = "State is required")
    protected State state;
}
