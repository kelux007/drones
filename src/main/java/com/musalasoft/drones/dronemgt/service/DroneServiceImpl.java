/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 8:13 PM
 *
 */

package com.musalasoft.drones.dronemgt.service;


import com.musalasoft.drones.audit.enums.AuditType;
import com.musalasoft.drones.audit.model.AuditEvent;
import com.musalasoft.drones.audit.repository.AuditEventRepository;
import com.musalasoft.drones.constants.ResponseConstant;
import com.musalasoft.drones.dronemgt.dto.DroneRegisterRequest;
import com.musalasoft.drones.dronemgt.dto.DroneResponseDto;
import com.musalasoft.drones.dronemgt.dto.DroneUpdateRequest;
import com.musalasoft.drones.dronemgt.enums.State;
import com.musalasoft.drones.dronemgt.model.Drone;
import com.musalasoft.drones.dronemgt.projection.DroneBatteryInfo;
import com.musalasoft.drones.dronemgt.repository.DroneRepository;
import com.musalasoft.drones.dto.ResponseDto;
import com.musalasoft.drones.util.LocaleHandler;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
@Slf4j
@RequiredArgsConstructor
public class DroneServiceImpl implements DroneService{

    private final DroneRepository droneRepository;

    private final ModelMapper mapper;

    private final AuditEventRepository auditEventRepository;
    @Override
    public ResponseDto register(DroneRegisterRequest droneRegisterRequest) {
        Drone drone = mapper.map(droneRegisterRequest, Drone.class);
        Drone savedDrone = droneRepository.save(drone);

        AuditEvent auditEvent =  new AuditEvent();
        auditEvent.setType(AuditType.CREATE_DRONE);
        auditEvent.setDetails(savedDrone.toString());
        auditEventRepository.save(auditEvent);

        return ResponseDto.builder()
                .respBody(savedDrone)
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respDescription(LocaleHandler.getMessage(ResponseConstant.DRONE_REGISTERED,null))
                .build();
    }

    @Override
    public ResponseDto update(DroneUpdateRequest droneUpdateRequest) {
        Drone drone = droneRepository.findFirstBySerialNumber(droneUpdateRequest.getSerialNumber());
        if(Objects.isNull(drone)){
            return ResponseDto.builder()
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.DRONE_UPDATE_FAILED))
                    .respCode(ResponseConstant.FAILED_CODE)
                    .build();
        }
        if(!Objects.isNull(droneUpdateRequest.getModel())){
            drone.setModel(droneUpdateRequest.getModel());
        }
        if(!Objects.isNull(droneUpdateRequest.getWeightLimit())){
            drone.setWeightLimit(droneUpdateRequest.getWeightLimit());
        }
        if(!Objects.isNull(droneUpdateRequest.getBatteryCapacity())){
            drone.setBatteryCapacity(droneUpdateRequest.getBatteryCapacity());
        }
        if(!Objects.isNull(droneUpdateRequest.getState())){
            drone.setState(droneUpdateRequest.getState());
        }

        Drone updatedDrone = droneRepository.save(drone);
        return ResponseDto.builder()
                .respBody(updatedDrone)
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respDescription(LocaleHandler.getMessage(ResponseConstant.DRONE_UPDATED,null))
                .build();
    }

    @Override
    public ResponseDto fetchByState(State state) {
        List<Drone> drones = droneRepository.findByState(state);
        if(drones.isEmpty()){
            return ResponseDto.builder()
                    .respCode(ResponseConstant.FAILED_CODE)
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.RECORD_NOT_FOUND,null))
                    .build();
        }
        return ResponseDto.builder()
                .respBody(drones)
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respDescription(LocaleHandler.getMessage(ResponseConstant.RECORD_FETCHED,null))
                .build();
    }

    @Override
    public ResponseDto checkBatteryLevel(String serialNo) {
        DroneBatteryInfo droneBatteryInfo = droneRepository.findBySerialNumber(serialNo);
        if(Objects.isNull(droneBatteryInfo)){
            return ResponseDto.builder()
                    .respCode(ResponseConstant.FAILED_CODE)
                    .respDescription(LocaleHandler.getMessage(ResponseConstant.RECORD_NOT_FOUND,null))
                    .build();
        }
        DroneResponseDto droneResponseDto = DroneResponseDto.builder()
                .batteryCapacity(droneBatteryInfo.getBatteryCapacity())
                .serialNumber(droneBatteryInfo.getSerialNumber())
                .build();

        return ResponseDto.builder()
                .respBody(droneResponseDto)
                .respCode(ResponseConstant.SUCCESS_CODE)
                .respDescription(LocaleHandler.getMessage(ResponseConstant.RECORD_FETCHED,null))
                .build();
    }

}
