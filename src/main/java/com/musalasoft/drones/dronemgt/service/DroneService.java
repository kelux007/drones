/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/23/22, 2:56 AM
 *
 */

package com.musalasoft.drones.dronemgt.service;

import com.musalasoft.drones.dronemgt.dto.DroneRegisterRequest;
import com.musalasoft.drones.dronemgt.dto.DroneUpdateRequest;
import com.musalasoft.drones.dronemgt.enums.State;
import com.musalasoft.drones.dto.ResponseDto;

public interface DroneService {
    ResponseDto register(DroneRegisterRequest droneRegisterRequest);

    ResponseDto update(DroneUpdateRequest droneUpdateRequest);

    ResponseDto fetchByState(State state);

    ResponseDto checkBatteryLevel(String serialNo);
}
