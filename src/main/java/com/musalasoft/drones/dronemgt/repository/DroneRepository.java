/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 5:43 PM
 *
 */

package com.musalasoft.drones.dronemgt.repository;

import com.musalasoft.drones.dronemgt.enums.State;
import com.musalasoft.drones.dronemgt.model.Drone;
import com.musalasoft.drones.dronemgt.projection.DroneBatteryInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DroneRepository extends JpaRepository<Drone, Long> {
    Drone findFirstBySerialNumber(String serialNo);

    List<Drone> findByState(State state);

    DroneBatteryInfo findBySerialNumber(String serialNo);

}
