/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dronemgt.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.musalasoft.drones.abstractmodel.AbstractEntity;
import com.musalasoft.drones.dronemgt.enums.Model;
import com.musalasoft.drones.dronemgt.enums.State;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Table(name = "drone")
@Setter
@Getter
@ToString(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)

public class Drone extends AbstractEntity {

    @Column(unique = true)
    protected String serialNumber;

    @Enumerated(EnumType.STRING)
    protected Model model;

    protected Integer weightLimit;

    protected Integer batteryCapacity;

    @Enumerated(EnumType.STRING)
    protected State state;

}
