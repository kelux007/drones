/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 5:43 PM
 *
 */

package com.musalasoft.drones.dronemgt.projection;

public interface DroneBatteryInfo {
    String getSerialNumber();

    Integer getBatteryCapacity();
}
