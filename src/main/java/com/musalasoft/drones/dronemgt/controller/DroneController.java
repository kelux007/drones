/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/24/22, 10:27 PM
 *
 */

package com.musalasoft.drones.dronemgt.controller;


import com.musalasoft.drones.dronemgt.dto.DroneRegisterRequest;
import com.musalasoft.drones.dronemgt.dto.DroneUpdateRequest;
import com.musalasoft.drones.dronemgt.enums.State;
import com.musalasoft.drones.dronemgt.service.DroneService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@RestController
@Slf4j
@RequestMapping("/drone")
@Validated
public class DroneController {

    @Autowired
    DroneService droneService;


    @PostMapping("/register")
    ResponseEntity<?> registerDrone(@Valid @RequestBody DroneRegisterRequest droneRegisterRequest) {
        log.info("Register drone request:{}", droneRegisterRequest);

        try {
            return new ResponseEntity<>(droneService.register(droneRegisterRequest), HttpStatus.CREATED);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


    }

    @PostMapping("/update")
    ResponseEntity<?> updateDrone(@Valid @RequestBody DroneUpdateRequest droneUpdateRequest) {
        log.info("Update drone request:{}", droneUpdateRequest);

        try {
            return new ResponseEntity<>(droneService.update(droneUpdateRequest), HttpStatus.OK);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


    }


    @GetMapping("/fetch/available")
    ResponseEntity<?> fetchAvailableDrones(){
        log.info("Fetch drones with state:{}", State.IDLE);

        try {
            return new ResponseEntity<>(droneService.fetchByState(State.IDLE), HttpStatus.OK);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


    }

    @GetMapping("/battery")
    ResponseEntity<?> checkBatteryLevel(@RequestParam @NotNull String serialNo){
        log.info("Fetch drone battery level for drone with serial number:{}", serialNo);

        try {
            return new ResponseEntity<>(droneService.checkBatteryLevel(serialNo), HttpStatus.OK);
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }


    }
}
