/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/23/22, 12:23 AM
 *
 */

package com.musalasoft.drones.dronemgt.enums;

public enum State {
    IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING
}
