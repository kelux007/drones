/*
 * *
 *  * Created by Kolawole Omirin
 *  * Copyright (c) 2022 . All rights reserved.
 *  * Last modified 9/23/22, 1:26 AM
 *
 */

package com.musalasoft.drones.util;

import java.util.Locale;
import java.util.Objects;
import java.util.ResourceBundle;

public class LocaleHandler {

    public static String getMessage(String messageKey, Locale locale){
        if(Objects.isNull(locale))
            locale = Locale.ENGLISH;
        return ResourceBundle.getBundle("appmessages", locale).getString(messageKey);
    }

    public static String getMessage(String messageKey){
        return getMessage(messageKey, null);
    }
}
