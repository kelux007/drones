
# Musala Soft Drone Service

Service to manage medication distribution using drones


## Documentation

Click here to access postman collection -->[Documentation](https://documenter.getpostman.com/view/4644795/2s83KW9Nkt)


## Build Project

1. Clone the repository or download and extract the archive file to your local directory.
2. Build a JAR file, run the command below

```bash
 $ mvn clean package
```

## How to run service locally

To run this project run the JAR file using the command below
```bash
 $ java -jar drones.jar
```


## How to run service using Docker
As Prerequisites, you must have `docker` and `docker-compose` installed on your machine; see
[Docker Install](https://docs.docker.com/install/) and
[Docker Compose Install](https://docs.docker.com/compose/install/).

1. Clone the repository or download and extract the archive file to your local directory.
2. Run the command below
```bash
 $ docker build -t drones . && docker run -p 8080:5050 --name Drones drones 
```



## Running Tests

To run tests, run the following command

```bash
 $ mvn test
```

