#
# Build stage
#

FROM maven:3.6.0-jdk-11-slim AS build

COPY src /app/src

WORKDIR /app

COPY pom.xml /app

RUN mvn clean install -DskipTests

RUN ls -lh /app/target

#
# Run stage
#
FROM openjdk:11-jre-slim

WORKDIR /app


COPY --from=build /app/target/drones.jar drones.jar

EXPOSE 5050

ENTRYPOINT ["java","-jar","drones.jar"]
